﻿using System.Collections.Generic;

namespace SystemDependencies
{
    public class Component
    {
        public string Name { get; set; }
        public List<string> DependName { get; set; } = new List<string>();

        public override string ToString() // just using now to print console
        {
            string msg = "Component Name : " + Name + "\n";
            foreach (string dn in DependName)
            {
                msg += "Depend Components' Name: " + dn + "\n";
            }
            return msg;
        }
    }
}