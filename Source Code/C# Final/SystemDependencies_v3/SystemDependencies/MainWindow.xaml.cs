﻿using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace SystemDependencies
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        VM vm = new VM();
        private string SAMPLE_FILE = "inputSample.txt";

        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
            CmdLine.Focus();
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                if (sender is TextBox)
                {
                    TextBox txb = (TextBox)sender;
                    vm.addInputCMD(txb.Text);
                    CmdLine.Text = "";
                    ScrollInto();
                }
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e) => vm.PrintLines.Clear();

        private void BtnSample_Click(object sender, RoutedEventArgs e)
        {
            string[] cmds = File.ReadAllLines(SAMPLE_FILE);
            foreach (string cmd in cmds)
            {
                vm.addInputCMD(cmd);
            }
            ScrollInto();
        }

        public void ScrollInto()
        {
            CmdShow.SelectedIndex = CmdShow.Items.Count - 1;
            CmdShow.ScrollIntoView(CmdShow.SelectedItem);
        }
    }
}
