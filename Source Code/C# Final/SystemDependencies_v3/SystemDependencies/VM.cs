﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SystemDependencies
{
    public class VM : INotifyPropertyChanged
    {
        public BindingList<Component> Components { get; set; } = new BindingList<Component>();
        public BindingList<string> InstalledComponents = new BindingList<string>();
        public BindingList<string> PrintLines { get; set; } = new BindingList<string>();

        const string SYS = "     ";
        const string DEPEND = "DEPEND";
        const string INSTALL = "INSTALL";
        const string REMOVE = "REMOVE";
        const string LIST = "LIST";
        const string END = "END";
        const int COMMAND_LIMIT = 80;
        const int COMPONENT_LIMIT = 10;

        public void addInputCMD(string cmd)
        {
            if (cmd.Length < COMMAND_LIMIT)
            {
                PrintLines.Add("User > " + cmd);

                string[] split = cmd.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                string command = split[0].Trim();

                for (var i = 1; i < split.Length; i++)
                {
                    if (split[i].Length > COMPONENT_LIMIT)
                    {
                        // need to remove from split
                        PrintLines.Add(SYS + "Component Character limit (" + COMPONENT_LIMIT + ") has been exceeded");
                    }
                }

                switch (command)
                {
                    case DEPEND:
                        Depend(split);
                        break;
                    case INSTALL:
                        Install(split);
                        break;
                    case REMOVE:
                        Remove(split);
                        break;
                    case LIST:
                        List(split);
                        break;
                    case END:
                        End(split);
                        break;
                    default:
                        Console.WriteLine("Undefined command");
                        break;
                }
            }
            else
            {
                PrintLines.Add(SYS + "Command character limit (" + COMMAND_LIMIT + ") has been exceeded");
            }
        }

        public void Depend(string[] components)
        {
            Component c = new Component();
            string addComponent = components[1].Trim();
            c.Name = addComponent;

            for ( var i = 2; i < components.Length; i++)
            {
                c.DependName.Add(components[i].Trim());
            }
            Components.Add(c);
        }

        public void Install(string[] components)
        {
            if (components.Length == 1)
                PrintLines.Add(SYS + "Component is required");
            else
            {
                string installComponent = components[1].Trim();
                if (!InstalledComponents.Contains(installComponent))
                {
                    CheckInstalled(installComponent);
                    InstalledComponents.Add(installComponent);
                    PrintLines.Add(SYS + installComponent + " is successfully installed");
                }
                else
                    PrintLines.Add(SYS + installComponent + " is already installed");
            }
        }

        public void List(string[] components)
        {
            if (InstalledComponents.Count > 0)
            {
                foreach (string ic in InstalledComponents)
                {
                    PrintLines.Add(SYS + ic + " is installed");
                }
            }
            else
                PrintLines.Add(SYS + "Any component is installed yet");
        }

        public void Remove(string[] components)
        {
            string removeComponent = components[1].Trim();
            CheckRemove(removeComponent);
        }

        public void End(string[] components)
        {
            // 
        }

        public void CheckInstalled(string s)
        {
            foreach (Component c in Components)
            {
                if (c.Name == s)
                {
                    foreach (string dc in c.DependName)
                    {
                        CheckInstalled(dc);
                        if (!InstalledComponents.Contains(dc))
                        {
                            InstalledComponents.Add(dc);
                            PrintLines.Add(SYS + dc + " is required and installed");
                        }
                    }
                }
            }
        }

        public void CheckRemove(string s)
        {
            if (InstalledComponents.Contains(s))
            {
                int count = 0;
                foreach (Component c in Components)
                {
                    Console.WriteLine("Components: " + c.Name);
                    if (c.DependName.Contains(s) && c.Name == s)
                    {
                        PrintLines.Add(s + " is still needed because of installed " + c.Name);
                        count += 1;
                    }
                    if (c.Name.Contains(s))
                    {
                        foreach (string dc in c.DependName)
                        {
                            CheckRemove(dc);
                            if (c.DependName.Contains(dc))
                            {
                                Console.WriteLine(dc + c.DependName);
                                count += 1;
                            }
                        }
                    }
                }
                if (count == 0)
                {
                    Console.WriteLine("REMOVE: " + s);
                    InstalledComponents.Remove(s);
                    PrintLines.Add(s + " is removed");
                }
            }
            else
                PrintLines.Add(SYS + s + " is not installed before");
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void notifyChange([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
